
var skierAssets = [
    "skierCrash",
    "skierLeft",
    "skierLeftDown",
    "skierDown",
    "skierRightDown",
    "skierRight",
    "skierJump1",
    "skierJump2",
    "skierJump3",
    "skierJump4",
    "skierJump5"
]

function getSkierAsset(skierDirection) {
    return skierAssets[skierDirection];
}

exports._test = {
    getSkierAsset: getSkierAsset
}


