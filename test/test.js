var assert = require('assert');
const skier = require('../js/skier.js');
describe('getSkierAsset', function() {
    it('should return "skierCrash" when the skierDirection is 0', function() {
        assert.equal(skier._test.getSkierAsset(0), "skierCrash");
    });

    it('should return "skierLeft" when the skierDirection is 1', function() {
        assert.equal(skier._test.getSkierAsset(1), "skierLeft");
    });

    it('should return "skierLeftDown" when the skierDirection is 2', function() {
        assert.equal(skier._test.getSkierAsset(2), "skierLeftDown");
    });

    it('should return "skierDown" when the skierDirection is 3', function() {
        assert.equal(skier._test.getSkierAsset(3), "skierDown");
    });

    it('should return "skierRightDown" when the skierDirection is 4', function() {
        assert.equal(skier._test.getSkierAsset(4), "skierRightDown");
    });

    it('should return "skierRight" when the skierDirection is 5', function() {
        assert.equal(skier._test.getSkierAsset(5), "skierRight");
    });

});